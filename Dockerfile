FROM php:fpm

RUN apt-get update
RUN apt-get install zlib1g-dev
RUN docker-php-ext-install zip 